import React from 'react';
import {  } from "material-ui"

import Nav from "../../component/nav/SideNav"
import Post from "../../component/post/Post";
// import CreatePost from "../../component/createpost/CreatePost";
import update from "immutability-helper";

import {Card, TextField, CardActions} from "material-ui";
import { createPost } from "../../api/post";

import { getAllPosts } from "../../api/post"
import { browserHistory } from "react-router";

import "./Dashboard.css"

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props)

    this.handleEnterSubmit = this.handleEnterSubmit.bind(this)
    this.handleTextChange = this.handleTextChange.bind(this)

    this.state = {
      posts: [],
      pendingPostName: "",
      pendingPostContent: "",
      error: {
        pendingPostName: "",
        pendingPostContent: ""
      }
    }
  }

  componentDidMount(){
    getAllPosts().then((response) => {
      response.data.reverse();
      this.setState({ posts: response.data })
    })
    .catch((err) => {
      browserHistory.push("/login")
    })
  }

  handleTextChange(e){
    this.setState({ [e.target.id]: e.target.value })
  }

  // updatePosts(value){
  //   this.setState({ posts: update(this.state.posts, { $push: [value] })})
  // }


  handleEnterSubmit(e){

    if (e.charCode === 13 && (this.state.pendingPostName.trim().length > 0 && this.state.pendingPostContent.trim().length > 0)) {
      createPost(this.state.pendingPostName, this.state.pendingPostContent).then((response) => {

        this.setState({
          posts: update(this.state.posts, {$unshift: [response.data]})
        })
        // this.setState({ posts: update(this.state.posts, { $unshift: [value] })})
        console.log(response.data)
        this.setState({pendingPostContent: "",pendingPostName:"", error: { pendingPostContent: "", pendingPostName: "" }})
      })

      this.setState({pendingPostContent: "",pendingPostName:"", error: { pendingPostContent: "", pendingPostName: "" }})
    }else if (e.charCode === 13) {
      const emptyField = "This field cannot be empty";
      const pendingPostName = ""
      const pendingPostContent = ""

      if (this.state.pendingPostName.trim().length === 0) {
        pendingPostName = emptyField
      }else{
        pendingPostName = ""
      }

      if (this.state.pendingPostContent.trim().length === 0){
        // this.setState({error: { pendingPostContent: emptyField }})
        pendingPostContent = emptyField
      }else {
        pendingPostContent = ""
      }
      this.setState({error: { pendingPostContent, pendingPostName }})

    }
  }

  render(){
    return (
      <div className="menuBarContainer">
        <Nav />

        <div className="postContainer">
          <Card>
            <CardActions>
              <TextField
                id="pendingPostName"
                hintText="Press Enter to Submit"
                value={this.state.pendingPostName}
                onChange={this.handleTextChange}
                floatingLabelText="Post Name"
                fullWidth={true}
                onKeyPress={this.handleEnterSubmit}
                errorText={this.state.error.pendingPostName}
                />

              <TextField
                id="pendingPostContent"
                hintText="Press Enter to Submit"
                onChange={this.handleTextChange}
                value={this.state.pendingPostContent}
                floatingLabelText="What's on your mind?"
                fullWidth={true}
                onKeyPress={this.handleEnterSubmit}
                errorText={this.state.error.pendingPostContent}
                />
            </CardActions>
          </Card>
        </div>
        {
          this.state.posts.map((post) => {
            return (<Post key={post.id} postId={post.id} postName={post.name} content={post.content} updatedAt={post.updated_at} />);
          })
      }
      </div>
    );
  }
}
