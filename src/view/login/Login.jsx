import React from "react";
import { TextField, Paper, RaisedButton } from "material-ui";
import {Row, Col } from 'react-flexbox-grid';
import { login, checkSignIn} from "../../api/login"
// import { waterfall } from 'async'
import {browserHistory} from "react-router";
import "./Login.css"

export default class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: "",
      password: ""
    }

    this.handleInputTextUpdateEvent = this.handleInputTextUpdateEvent.bind(this)
    this.authenticate = this.authenticate.bind(this)
  }

  componentDidMount(){
    checkSignIn().then((response) => {
      browserHistory.push("/post")
    })
  }

  authenticate(){
    login(this.state.email, this.state.password)
      .then((response) => {
        if (response.status === 200){
          browserHistory.push("/post")
        }else {
          console.log(response.status)
        }
      })
  }

  handleInputTextUpdateEvent(event){
    this.setState({ [event.target.id]: event.target.value })
  }

  render(){
    return (
      <Paper className="loginContainer">
        <Row>
          <Col xs={12} >
            <h2>Sign In</h2>
          </Col>
        </Row>

        <Row>
          <Col xs={12} md={6}>
            <TextField
              hintText="Email Field"
              floatingLabelText="Email"
              value={this.state.email}
              id="email"
              onChange={this.handleInputTextUpdateEvent}
              />
          </Col>
          <Col xs={12} md={6}>
            <TextField
              hintText="Password Field"
              floatingLabelText="Password"
              type="password"
              value={this.state.password}
              id="password"
              onChange={this.handleInputTextUpdateEvent}
              />
          </Col>
        </Row>

        <Row>
          <Col xs={12}>
            <RaisedButton
              label="Sign In"
              secondary={true}
              fullWidth={true}
              onClick={this.authenticate}
              />
          </Col>
        </Row>
    </Paper>
  );
  }
}
