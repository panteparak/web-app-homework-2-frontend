import Axios from "./AxiosConfiguration";
import Promise from "promise";
import {browserHistory} from 'react-router';

function getAllPosts(){
  return new Promise((accept, rejected) => {
    Axios.get("/posts.json")
      .then((response) => {
        const {status, data} = response
        accept({status, data})
      })
      .catch((err) => {
        browserHistory.push("/login")
        rejected()
      })
  })
}

function getPostById(id){
  return new Promise((accept, rejected) => {
    Axios.get(`/posts/${id}.json`).then((response) => {
      const { data, status } = response
      accept({data, status})
    })
    .catch((err) => {
      browserHistory.push("/login");
      rejected()
    })
  })
}

function createPost(name, content){
  return new Promise((accept, rejected) => {
    Axios.post("/posts.json", { post: { name, content } })
      .then((response) => {
        const { status, data} = response
        accept( { status, data } )
      })
      .catch((err) => {
        browserHistory.push("/login")
        rejected()
      })
  })
}

export { getAllPosts, getPostById, createPost }
