import Axios from "axios";


export default Axios.create({
  baseURL: 'https://iccs340-vue-api-auth.herokuapp.com',
  withCredentials: true,
  headers: {
    common: {
      Accept: 'application/json'
    },
  }
})
