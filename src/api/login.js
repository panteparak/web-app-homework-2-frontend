import Axios from './AxiosConfiguration';
import Promise from "promise";
import {browserHistory} from  "react-router";

function login(email, password){
  return new Promise((accept, rejected) => {
    Axios.post("/users/api_sign_in.json", {user: {email, password}})
      .then((response) => {
        const { status, data } = response;
        accept({status, data})
      })
      .catch((err) => {
        rejected()
      })
  })
}

function logout(){
  return new Promise((accept, rejected) => {
    Axios.delete("/users/api_sign_out.json")
      .then((response) => {
        accept()
        browserHistory.push("/login")
      })
      .catch((err) => {
        rejected()
      })
  })

}

function checkSignIn(){
  return new Promise((accept, rejected) => {
    Axios.get("/users/check_signed_in.json")
      .then((response) => {
          accept()
      })
      .catch((err) => {
        logout()
        rejected()
      })
  })
}



export { login, logout, checkSignIn };
