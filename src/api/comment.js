import Axios from "./AxiosConfiguration";
import Promise from "promise"
import {browserHistory} from "react-router";

function getCommentByPostId(postId){
  return new Promise((accept, rejected) => {
    Axios.get(`/posts/${postId}/comments.json`)
      .then((response) => {
        const { status, data } = response;
        accept({ status, data })
      })
      .catch((err) => {
        // const { status, data } = err;
        browserHistory.push("/login")
        rejected()
      })
  })
}

function addCommentToPost(postId, content){
  return new Promise((accept, rejected) => {
    Axios.post(`/posts/${postId}/comments.json`, {comment: {content}})
      .then((response) => {
        const { status, data } = response;
        accept({ status, data })
      })
      .catch((err) => {
        // const { status, data } = err;
        browserHistory.push("/login")
        rejected()
      })
  })
}

export {getCommentByPostId, addCommentToPost}
