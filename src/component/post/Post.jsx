import React from 'react';
import Comment from '../comment/Comment'
import {Card, CardHeader,  CardText, TextField, CardActions} from "material-ui";
import {addCommentToPost, getCommentByPostId} from "../../api/comment";
import timeago from 'time-ago'
import update from "immutability-helper";

import "./Post.css";

export default class Post extends React.Component {
  constructor(props){
    super(props)
    this.handleExpandChange = this.handleExpandChange.bind(this)
    this.handleTextChange = this.handleTextChange.bind(this)
    this.handleSubmitComment = this.handleSubmitComment.bind(this)

    this.state = {
      expanded: false,
      post: {
        id: props.postId,
        name: props.postName,
        content: props.content,
        updatedAt: timeago().ago(props.updatedAt)
      },
      comments: [],
      pendingComment: ""
    }
  }


  handleExpandChange(expanded){
      this.setState({expanded})

      if (expanded) {
        getCommentByPostId(this.state.post.id).then((response) => {
          this.setState({comments: response.data})
        })
      }
  }


  handleTextChange(e){
    this.setState({[e.target.id]: e.target.value})
  }

  handleSubmitComment(e){
    if (e.charCode === 13 && this.state.pendingComment.trim().length > 0) {
      // console.log("Enter Pressed")
      addCommentToPost(this.state.post.id, this.state.pendingComment).then((response) => {
        this.setState({
          comments: update(this.state.comments, {$push: [response.data]})
        })
        this.setState({pendingComment: ""})
      })
    }
  }

  render(){
    return (
      <div className="postContainer">
        <Card expanded={this.state.expanded} onExpandChange={this.handleExpandChange} >
          <CardHeader
            title={this.state.post.name}
            subtitle={"Last Updated: " + this.state.post.updatedAt}
            actAsExpander={true}
            showExpandableButton={true}
          />
        <CardText><h2>{this.state.post.content}</h2></CardText>
          <CardText expandable={true}>
            {this.state.comments.length > 0 ?
              (
                  this.state.comments.map((comment) => {
                    return (<Comment
                              key={comment.id}
                              commentorEmail={comment.user.email}
                              content={comment.content}
                              updatedAt={comment.updated_at}
                              />)
                  })
              ) : ("No Comment")
            }
          </CardText>
          <CardActions>
            <TextField
              id="pendingComment"
              hintText="Press Enter to Submit"
              onChange={this.handleTextChange}
              floatingLabelText="Write a comment..."
              fullWidth={true}
              onKeyPress={this.handleSubmitComment}
              value={this.state.pendingComment}
              />
          </CardActions>
        </Card>
      </div>

    );
  }

}
