import React from "react";
import {AppBar, Drawer, MenuItem} from "material-ui";
import {logout} from "../../api/login";

export default class SideNav extends React.Component {
  constructor(props) {
    super(props)
    this.toggleSideNav = this.toggleSideNav.bind(this)
    this.toggleLogout = this.toggleLogout.bind(this)

    this.state = {
      open: false
    }
  }

  toggleLogout(){
    this.toggleSideNav()
    logout()
  }

  toggleSideNav(){
    this.setState({
      open: !this.state.open
    })
  }

  render(){
    return (
      <div>
        <AppBar title="Posts" onLeftIconButtonTouchTap={ this.toggleSideNav }/>
        <Drawer open={this.state.open} docked={false} onRequestChange={(open) => this.setState({open})} >
          <MenuItem onClick={this.toggleLogout}>Logout</MenuItem>
        </Drawer>
      </div>
    );
  }
}
