import React from "react";
import {Card, CardTitle, CardText} from "material-ui";
import timeago from 'time-ago'



export default class Comment extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      comment: {
        // id: props.commentId,
        email: props.commentorEmail,
        content: props.content,
        updatedAt: timeago().ago(props.updatedAt)
      }
    }
  }

  render(){
    return (
      <Card>
      <CardTitle title={this.state.comment.email} subtitle={this.state.comment.updatedAt} />
        <CardText>
          {this.state.comment.content}
        </CardText>
      </Card>
    )
  }
}
