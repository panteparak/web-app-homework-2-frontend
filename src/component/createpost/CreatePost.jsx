import React from "react";
import "./CreatePost.css";
import {browserHistory} from "react-router";
import {Card, TextField, CardActions} from "material-ui";
import { createPost } from "../../api/post";
import update from "immutability-helper";

export default class CreatePost extends React.Component {

  constructor(props){
    super(props)

    this.handleEnterSubmit = this.handleEnterSubmit.bind(this)
    this.handleTextChange = this.handleTextChange.bind(this)
    // console.log(props.posts)
    this.state = {
      pendingPostName: "",
      pendingPostContent: "",
      error: {
        pendingPostName: "",
        pendingPostContent: ""
      }
    }
  }

  handleTextChange(e){
    this.setState({ [e.target.id]: e.target.value })
  }



  handleEnterSubmit(e){

    if (e.charCode === 13 && (this.state.pendingPostName.trim().length > 0 && this.state.pendingPostContent.trim().length > 0)) {
      createPost(this.state.pendingPostName, this.state.pendingPostContent).then((response) => {
        console.log(response.data)
        // this.setState({
        //   posts: update(this.props.posts, {$push: [response.data]})
        // })

        this.props.posts(response.data)
        console.log(response.data)
        this.setState({pendingPostContent: "",pendingPostName:"", error: { pendingPostContent: "", pendingPostName: "" }})
      })

      this.setState({pendingPostContent: "",pendingPostName:"", error: { pendingPostContent: "", pendingPostName: "" }})
    }else if (e.charCode === 13) {
      const emptyField = "This field cannot be empty";
      const pendingPostName = ""
      const pendingPostContent = ""

      if (this.state.pendingPostName.trim().length === 0) {
        pendingPostName = emptyField
      }else{
        pendingPostName = ""
      }

      if (this.state.pendingPostContent.trim().length === 0){
        // this.setState({error: { pendingPostContent: emptyField }})
        pendingPostContent = emptyField
      }else {
        pendingPostContent = ""
      }
      this.setState({error: { pendingPostContent, pendingPostName }})

    }
  }

  render(){
    return (
      <div className="postContainer">
        <Card>
          <CardActions>
            <TextField
              id="pendingPostName"
              hintText="Press Enter to Submit"
              value={this.state.pendingPostName}
              onChange={this.handleTextChange}
              floatingLabelText="Post Name"
              fullWidth={true}
              onKeyPress={this.handleEnterSubmit}
              errorText={this.state.error.pendingPostName}
              />

            <TextField
              id="pendingPostContent"
              hintText="Press Enter to Submit"
              onChange={this.handleTextChange}
              value={this.state.pendingPostContent}
              floatingLabelText="What's on your mind?"
              fullWidth={true}
              onKeyPress={this.handleEnterSubmit}
              errorText={this.state.error.pendingPostContent}
              />
          </CardActions>
        </Card>
      </div>
    );
  }
}
