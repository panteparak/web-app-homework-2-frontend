import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
// import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import Login from './view/login/Login';
import Dashboard from './view/dashboard/Dashboard';
// import SideNav from './component/nav/SideNav';

import './index.css';

injectTapEventPlugin();

import {Router , Route, browserHistory, Redirect } from 'react-router'

function App(){
  return (
    <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
      <Router history={browserHistory}>
        <Route path="/login" component={Login} />
        <Route path="/post" component={Dashboard} />
        <Redirect from="/" to="/login"/>
      </Router>
    </MuiThemeProvider>
  )
}

// function Tester(){
//   return (
//     <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
//       <Dashboard />
//     </MuiThemeProvider>
//   )
// }

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
